from django_elasticsearch_dsl import Document, Index
from elasticsearch_dsl.connections import connections

from .models import *

# Define a default Elasticsearch client
connections.create_connection(hosts=['localhost'])
items = Index('items')


@items.doc_type
class ItemDocument(Document):
    class Django:
        model = Item
        fields = [
            "title",
            "url",
            "rating",
            "imageURL",
            "year",
            "id"
        ]
