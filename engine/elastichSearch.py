from elasticsearch import Elasticsearch
import csv


es = Elasticsearch()

def connectToES(_host, _port):
    es =Elasticsearch([{'host' : _host, 'port': _port}])


def indexElasticSearch(dataSetName): #index Elastic Search
    with open(dataSetName, 'r') as DS:
        readear = csv.reader(DS)
        next(readear)
        i= 1
        for line in readear:
            es.index(index='database1',doc_type='film',id=i,body={'tconst':line[0],'titleType':line[1],'primaryTitle': line[2],
            'originalTitle': line[3],'isAdult':line[4], 'startYear':line[5], 'endYear': line[6],'runtimeMinutes':line[7],'genres': line[8],
            'averageRating':line[9],'numVotes':line[10], 'poster':line[11], 'announce': line[12], 'summary': line[13], 'director': line[14],
            'writers': line[15],'stars':line[16]})
            i = i+1

#these function returns a json file
#use for item in collection :
    #item[_source][primaryTitle] return primaryTitle


def search(text):
    res = es.search(index='database1',doc_type='film', body={'size' : 2000,'query' :{'query_string':{ 'query':text,'fields' : ['primaryTitle','originalTitle','geners','stars']}}})
    return res['hits']['hits']
    
def filterByYear(min,max):
    res = es.search(index='database1',doc_type='film',body={'size' : 2000 ,'query' :  { 'bool':{'filter' : [{'range' : {'startYear' : {'from' : min,'to' : max}}}]}}})
    return res['hits']['hits']
    


def filterByAverageRating(min,max):
    res = es.search(index='database1',doc_type='film',body={'size' : 2000 ,'query' :  {'bool' :{'filter' : [{'range' : {'averageRating' : {'gte' : min,'lte' : max}}}]}}})
    return res['hits']['hits']



def filterByGenre(genre):
    res = es.search(index='database1', doc_type='film' , body={'size' : 2000 , 'query' : {'match' : {'genres':genre}}})
    return res['hits']['hits']
    