from django.contrib.auth.models import User
from django.db import models
from rest_framework import serializers


# Create your models here.

class Features(models.Model):
    action = models.IntegerField(default=0)
    adult = models.IntegerField(default=0)
    adventure = models.IntegerField(default=0)
    animation = models.IntegerField(default=0)
    biography = models.IntegerField(default=0)
    comedy = models.IntegerField(default=0)
    crime = models.IntegerField(default=0)
    documentary = models.IntegerField(default=0)
    drama = models.IntegerField(default=0)
    family = models.IntegerField(default=0)
    fantasy = models.IntegerField(default=0)
    gameShow = models.IntegerField(default=0)
    history = models.IntegerField(default=0)
    horror = models.IntegerField(default=0)
    music = models.IntegerField(default=0)
    musical = models.IntegerField(default=0)
    mystery = models.IntegerField(default=0)
    news = models.IntegerField(default=0)
    realitytv = models.IntegerField(default=0)
    romance = models.IntegerField(default=0)
    sci_fi = models.IntegerField(default=0)
    short = models.IntegerField(default=0)
    sport = models.IntegerField(default=0)
    talkShow = models.IntegerField(default=0)
    war = models.IntegerField(default=0)
    western = models.IntegerField(default=0)
    film_noir = models.IntegerField(default=0)
    thriller = models.IntegerField(default=0)


def mul(features, formula):
    """
    Instead of category by category compare, I used this to determine total value
    :param features: list of features of one user
    :param formula: normalized list as coefficients list
    :return: sum of multiplied matrix
    """
    x = sum(features[k] * formula[k] for k in range(0, len(features)))
    # print("X " + str(x))
    return x


class Artist(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return "%i, %s" % (self.id, self.name)


class Item(models.Model):
    title = models.CharField(max_length=255, default='')
    url = models.CharField(max_length=255, default='')
    imageURL = models.CharField(max_length=255, default='')
    announce = models.CharField(max_length=255, default='')
    summary = models.CharField(max_length=255, default='')
    rating = models.FloatField(default=1.0)
    year = models.IntegerField(default=2000)
    votes = models.IntegerField(default=0)
    features = models.ForeignKey(Features, on_delete=models.CASCADE, default=1)
    artists = models.ManyToManyField(Artist)
    formula = []

    def __str__(self):
        return "%i, %s, %s, %s, %d, %i" % (self.id, self.title, self.url, self.imageURL, self.rating, self.year)
        # ''.join(str(x) for x in self.formula)

    def __lt__(self, other):
        return mul(list(self.features.__dict__.values())[2:], self.formula) < \
               mul(list(other.features.__dict__.values())[2:], self.formula)


class Result(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, default=1)
    features = models.OneToOneField(Features, on_delete=models.CASCADE, default=1)

    def __str__(self):
        return "user id: %i, feature id: %i" % (self.user.id, self.features.id)


class Favorites(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, default=1)
    favs = models.ManyToManyField(Item)

    def __str__(self):
        x = "user id: " + str(self.user_id) + "\n"
        for item in self.favs.all():
            x += (item.title + "\n")
        return x


# class UserSerializer(serializers.ModelSerializer):
#     class Meta:
#         fields = ("username", "password", "email", "id")
#         model = User


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ("title", "url", "rating", "imageURL", "year", "id")
        model = Item

class subjectBoards(models.Model):
    boardName = models.CharField(max_length=31,primary_key = True,uniqeu = True)
    num_pins = models.IntegerField(default=0) #how many item pined in this board
    items = models.ManyToManyField(Item)
    createdDate = models.DateField()

    def __str__(self):
        return str({'boardName' : self.boardName,'num_pins':self.num_pins,'createdDate':self.createdDate})


