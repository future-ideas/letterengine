from django.contrib import messages
from django.core.paginator import Paginator
from django.http import JsonResponse
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status, permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication
from webdriver_manager.chrome import ChromeDriverManager

from .EmailTokens import account_activation_token
from .models import *
from .serializers import *


# Create your views here.

class UserCreate(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format='json'):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.is_active = False
            user = serializer.save()
            user.is_active = False
            user.save()
            if user:
                from django.contrib.sites.shortcuts import get_current_site
                from django.utils.encoding import force_bytes
                from django.utils.http import urlsafe_base64_encode
                from django.template.loader import render_to_string
                from .EmailTokens import account_activation_token
                from django.contrib.auth.models import User
                from django.core.mail import EmailMessage
                current_site = get_current_site(request)
                mail_subject = 'حساب نامه خود را فعال کنید'
                message = render_to_string('acc_active_email.html', {
                    'user': user,
                    'domain': current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'token': account_activation_token.make_token(user),
                })
                to_email = user.email
                email = EmailMessage(
                    mail_subject, message, to=[to_email]
                )
                email.send()
                return Response(data={"msg": "ایمیل فعال سازی ارسال شد، لطفا ایمیل خود را وارسی کنید"}, status=200)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    from django.http import HttpResponse
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        return HttpResponse('حساب نامه شما با موفقیت فعال شد :) <br>  لطفا ورود کنید')
    else:
        return HttpResponse('لینک فعال سازی معتبر نیست!')


def auth(request):
    user = JWTAuthentication().authenticate(request)[0]
    if not user:
        raise Exception()
    return user


def normalizer(user_features_list):
    """
    In order to normalize values of user's result between 0 to 1
    :param user_features_list: values of user's features
    :return: normalized user's features
    """
    import numpy as np
    np_array = np.array(user_features_list)
    normalized_v = np_array / np.sqrt(np.sum(np_array ** 2))
    return normalized_v


def new_result_dict(result, user_features_list):
    """
    Whenever we want to modify user feature, which is a dictionary
    :param result: The user result dictionary, using as the template
    :param user_features_list: Feature list, which must be written
    :return: The updated result dictionary
    """
    new_dic = {}
    _ = 0
    # Because of special values for 1th and 2th pairs in result.features we shall keep them unchanged
    for key, value in result.features.__dict__.items():
        if _ > 1:
            break
        new_dic[key] = value
        _ += 1
    # print(result.features.__dict__.items())
    for index, key in enumerate(list(result.features.__dict__.keys())[2:]):
        new_dic[key] = user_features_list[index]
    return new_dic


def suggest(user_id):
    """
    In order to provide paginated customized content to this user
    :param user_id: To distinguish user
    :return: 15 paginated items as a list
    """
    items = Item.objects.all().order_by('?')
    result = Result.objects.get(user_id=user_id)
    user_features_list = list(result.features.__dict__.values())[2:]
    formula = list(new_result_dict(result, normalizer(user_features_list)).values())[2:]
    for item in items:
        item.formula = formula
    sorted(items)
    paginator = Paginator(items, 1)
    return paginator


@csrf_exempt
def content(request):
    user = auth(request)
    # ItemDocument.search().query("match", title=q)
    paginator = suggest(user.id)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    print(page_obj)
    posts_serialized = ItemSerializer(page_obj.object_list, many=True)
    return JsonResponse(posts_serialized.data, safe=False)


def modify(user_id, item_id, score):
    try:
        result = Result.objects.get(user_id=user_id)
        item = Item.objects.get(pk=item_id)
        features = item.features
        features_list = list(features.__dict__.values())[2:]
        features_list = [i * score for i in features_list]
        user_features_list = list(result.features.__dict__.values())[2:]
        user_features_list = [features_list[i] + user_features_list[i] for i in range(len(features_list))]
        result.features.__dict__.update(new_result_dict(result, user_features_list))
        Features.save(result.features)
        Result.save(result)
        print(result.features.__dict__.values())
    except Result.DoesNotExist or Item.DoesNotExist:
        raise Exception("Not found result")


@csrf_exempt
def engage(request, score, *args):
    user = auth(request)
    item_id = request.POST['id']

    if 'like' in args:
        record = Favorites.objects.get_or_create(user=user)
        try:
            record[0].favs.add(Item.objects.get(id=item_id))
        except Item.DoesNotExist:
            return JsonResponse({'Error': "The Item Does Not Exist!"}, statu=400)
    try:
        modify(user.id, item_id, score)
        return JsonResponse({"msg": "Successfully modified"}, status=200, content_type="application/json")
    except Item.DoesNotExist:
        return JsonResponse({'Error': "invalid requested post"}, status="400")


@csrf_exempt
def seen(request):
    return engage(request, 1)


@csrf_exempt
def like(request):
    return engage(request, 3, 'like')


@csrf_exempt
def get_favorites(request):
    user = auth(request)
    from django.core import serializers
    print(Favorites.objects.get(user=user).favs.all().values())
    data = serializers.serialize('json', Favorites.objects.get(user=user).favs.all())
    print(data)
    from django.http import HttpResponse
    return HttpResponse(data, content_type="application/json")


@csrf_exempt
def download(request):
    path = './amir.html'
    from django.http import HttpResponse
    response = HttpResponse(open(path, 'rb').read())
    response['Content-Type'] = 'text/plain'
    response['Content-Disposition'] = 'attachment; filename=app.apk'
    return response


def string_to_set(string):
    array = string.split(",")
    s = set()
    for x in array:
        s.add(str(x).strip()[1:-1])
    return s


def read_csv(request):
    import pandas as pd
    df = pd.read_csv('dataset.csv')
    headers = []
    ind = 0
    for x in df:
        headers.append(x)
        ind += 1
        if ind > 16:
            break
    print(headers)
    for index, row in df.iterrows():
        for header in headers:
            print(row[header])
        genres = {}
        for y in str(row['genres']).strip().split(","):
            genre_name = str(y).lower()
            if genre_name == 'sci-fi':
                genre_name = "sci_fi"
            if genre_name == 'film-noir':
                genre_name = "film_noir"
            genres[genre_name] = 1
        artists = {row['director']}
        artists.update(string_to_set(row['writers'][1:-1]))
        artists.update(string_to_set(row['stars'][1:-1]))
        artists_objects = []
        for x in artists:
            artist, created = Artist.objects.get_or_create(name=x)
            artists_objects.append(artist)

        features = Features(**genres)
        features.save()
        item = Item(
            title=row['originalTitle'],
            url="https://www.imdb.com/title/" + row['tconst'],
            rating=row['averageRating'],
            year=row['startYear'],
            features=features,
            votes=row['numVotes'],
        )
        item.save()
        for artist in artists_objects:
            item.artists.add(artist)

    return JsonResponse({'msg': "The Feature Successfully added"}, status=200)
